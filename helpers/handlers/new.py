from telegram import InlineKeyboardMarkup

from helpers.messages import Messages
from helpers.common import buttons


def new_handler(update, context):
    update.message.reply_text(
        text=Messages.BANNER, reply_markup=InlineKeyboardMarkup(buttons), quote=True
    )
