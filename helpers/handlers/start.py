from telegram import ParseMode

from helpers.messages import Messages


def start_handler(update, context):
    """Send a message when the command /start is issued."""
    context.bot.send_message(
        chat_id=374321319,
        text=f"Hey, {update.message.from_user.mention_html()} started your bot",
        parse_mode=ParseMode.HTML,
    )
    update.message.reply_text(
        text=Messages.START_TEXT.format(update.message.from_user.mention_html()),
        quote=True,
        parse_mode=ParseMode.HTML,
    )
