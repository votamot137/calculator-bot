from uuid import uuid4

from telegram import InlineQueryResultArticle, InputTextMessageContent

from helpers.common import calc_expression


def get_allowed_expr(str_expr: str):
    allowed_chars = ["/", "*", "-", "+", "(", ")", "."]
    expr = ""
    for i in str_expr:
        if i.isdigit():
            expr += i
        elif i in allowed_chars:
            expr += i
    return expr


def inline_handler(update, contet):
    expression = update.inline_query.query.strip()
    results = []
    if expression:
        exp = get_allowed_expr(expression)
        result = str(calc_expression(exp)) or "Error"
        results.append(
            InlineQueryResultArticle(
                id=uuid4(),
                title="Result",
                description=result,
                input_message_content=InputTextMessageContent(
                    f"{expression} = {result}"
                ),
            )
        )
    else:
        results.append(
            InlineQueryResultArticle(
                id=uuid4(),
                title="Enter your expression",
                input_message_content=InputTextMessageContent("/start"),
            )
        )

    update.inline_query.answer(results=results, cache_time=10, is_personal=True)
