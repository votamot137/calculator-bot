import logging
import re

from telegram import InlineKeyboardMarkup

from helpers.messages import Messages
from helpers.common import buttons, calc_expression


logger = logging.getLogger(__name__)


def button_press_handler(update, context):
    """Function to handle the button press"""
    callback_query = update.callback_query
    callback_query.answer()
    text = callback_query.message.text.split("\n")[0].strip().split("=")[0].strip()
    text = "" if Messages.BANNER in text else text
    data = callback_query.data
    inpt = text + data
    result = ""
    if data == "=" and text:
        result = calc_expression(text)
        text = ""
    elif data == "DEL" and text:
        text = text[:-1]
    elif data == "AC":
        text = ""
    else:
        dot_dot_check = re.findall(r"(\d*\.\.|\d*\.\d+\.)", inpt)
        opcheck = re.findall(r"([*/\+-]{2,})", inpt)
        if not dot_dot_check and not opcheck:
            strOperands = re.findall(r"(\.\d+|\d+\.\d+|\d+)", inpt)
            if strOperands:
                text += data
                result = calc_expression(text)

    text = f"{text:<50}"
    if result:
        if text:
            text += f"\n{result:>50}"
        else:
            text = result
    text += "\n\n" + Messages.BANNER
    try:
        callback_query.edit_message_text(
            text=text, reply_markup=InlineKeyboardMarkup(buttons)
        )
    except Exception as e:
        logger.info(e)
        pass
