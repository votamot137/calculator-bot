class Messages:
    BANNER = "{:.^34}".format(" Calculator by @odbots ")

    START_TEXT = (
        "Hi {}.\n\n"
        "I'm a simple calculator bot. You can use me inline and also in interactive mode.\n\n"
        "> To use me in inline just type <code>@CalcIt_bot</code> followed by the expression.\n"
        "Eg: <code>@CalcIt_bot 22/7</code>\n\n"
        "> To use me in interactive mode, use /new command."
    )
