from telegram.ext import (
    Dispatcher,
    CommandHandler,
    CallbackQueryHandler,
    InlineQueryHandler,
)

from helpers.handlers import (
    start_handler,
    button_press_handler,
    new_handler,
    inline_handler,
)


def get_dispatcher(bot):
    """Create and return dispatcher instances"""
    dispatcher = Dispatcher(bot, None, workers=0)

    dispatcher.add_handler(CommandHandler("start", start_handler))
    dispatcher.add_handler(CommandHandler("new", new_handler))
    dispatcher.add_handler(CallbackQueryHandler(button_press_handler))
    dispatcher.add_handler(InlineQueryHandler(inline_handler))

    return dispatcher
